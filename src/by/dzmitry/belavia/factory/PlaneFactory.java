package by.dzmitry.belavia.factory;

import static by.dzmitry.belavia.parser.constant.ParserConstant.*;

import org.apache.log4j.Logger;

import by.dzmitry.belavia.entity.CargoPlane;
import by.dzmitry.belavia.entity.PassengerPlane;
import by.dzmitry.belavia.entity.Plane;

public class PlaneFactory {

	private static final Logger LOGGER = Logger.getLogger(PlaneFactory.class);

	public Plane create(String typePlane) { // throw exception

		Plane plane = null;

		switch (typePlane) {

		case CARGO_PLANE:
			plane = new CargoPlane();
			break;

		case PASSENGER_PLANE:
			plane = new PassengerPlane();
			break;

		default:
			LOGGER.error("The plane type (" + typePlane + ") is incorrect in create() method."); //throw exception, not logger
		}
		return plane;
	}
}
