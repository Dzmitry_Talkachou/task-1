package by.dzmitry.belavia.factory;

import static by.dzmitry.belavia.parser.constant.ParserConstant.*;

import org.apache.log4j.Logger;

import by.dzmitry.belavia.entity.CargoPlaneSpecificaton;
import by.dzmitry.belavia.entity.PassengerPlaneSpecification;
import by.dzmitry.belavia.entity.PlaneSpecification;

public class PlaneSpecificationFactory {

	private static final Logger LOGGER = Logger.getLogger(PlaneSpecificationFactory.class);

	public PlaneSpecification create(String type) {
		
		 PlaneSpecification planeSpecs = null;

		switch (type) {

		case CARGO_PLANE_SPEC:
			planeSpecs = new CargoPlaneSpecificaton();
			break;

		case PASSENGER_PLANE_SPEC:
			planeSpecs = new PassengerPlaneSpecification();
			break;

		default:
			LOGGER.error("The plane type (" + type + ") is incorrect in create() method.");
		}
		return planeSpecs;
	}
}
