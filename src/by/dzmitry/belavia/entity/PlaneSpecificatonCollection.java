package by.dzmitry.belavia.entity;

import java.util.ArrayList;

import by.dzmitry.belavia.parser.action.CollectPlaneSpecification;

public class PlaneSpecificatonCollection {

	private ArrayList<PlaneSpecification> planeSpecs;

	public PlaneSpecificatonCollection(String xmlTag, String filename, String[] fieldsXML) {

		this.planeSpecs = CollectPlaneSpecification.collect(xmlTag, filename, fieldsXML);

	}

	public boolean isEmpty() {

		return this.planeSpecs.isEmpty();
	}

	public int size() {

		return this.planeSpecs.size();
	}

	public PlaneSpecification get(int key) {
		return this.planeSpecs.get(key);
	}

	@Override
	public String toString() {
		return "PlaneSpecList [planeSpecs=" + planeSpecs + "]";
	}

}
