package by.dzmitry.belavia.entity;

public class PassengerPlaneSpecification extends PlaneSpecification {

	private int passengerSeats;

	public int getPassengerSeats() {
		return passengerSeats;
	}

	public void setPassengerSeats(int passengers) {
		this.passengerSeats = passengers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + passengerSeats;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PassengerPlaneSpecification other = (PassengerPlaneSpecification) obj;
		if (passengerSeats != other.passengerSeats)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", passengerSeats=" + passengerSeats + "]\n";
	}
}