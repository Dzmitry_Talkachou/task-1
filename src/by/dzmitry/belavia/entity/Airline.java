package by.dzmitry.belavia.entity;

import static by.dzmitry.belavia.parser.constant.ParserConstant.*;

public class Airline {

	private final PlaneSpecificatonCollection cargoPlanesSpecs = new PlaneSpecificatonCollection(CARGO_PLANE_SPEC,
			CARGO_PLANE_SPEC_FILE, CARGO_PLANE_SPEC_OPTIONS);

	private final PlaneSpecificatonCollection passengerPlanesSpecs = new PlaneSpecificatonCollection(PASSENGER_PLANE_SPEC,
			PASSENGER_PLANE_SPEC_FILE, PASSENGER_PLANE_SPEC_OPTIONS);

	private final PlaneCollection cargoPlanes = new PlaneCollection(CARGO_PLANE, CARGO_PLANE_FILE, CARGO_PLANE_OPTIONS,
			cargoPlanesSpecs);

	private final PlaneCollection passengerPlanes = new PlaneCollection(PASSENGER_PLANE, PASSENGER_PLANE_FILE,
			PASSENGER_PLANE_OPTIONS, passengerPlanesSpecs);

	private String name;

	public Airline(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PlaneSpecificatonCollection getCargoPlanesSpecs() {
		return cargoPlanesSpecs;
	}

	public PlaneSpecificatonCollection getPassengerPlanesSpecs() {
		return passengerPlanesSpecs;
	}

	public PlaneCollection getCargoPlanes() {
		return cargoPlanes;
	}

	public PlaneCollection getPassengerPlanes() {
		return passengerPlanes;
	}

}
