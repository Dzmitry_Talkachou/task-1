package by.dzmitry.belavia.entity;

public abstract class Plane {

	private String tailNumber;
	private PlaneSpecification planeSpec;

	public String getTailNumber() {
		return tailNumber;
	}

	public void setTailNumber(String tailNumber) {
		this.tailNumber = tailNumber;
	}

	public PlaneSpecification getPlaneSpec() {
		return planeSpec;
	}

	public void setPlaneSpec(PlaneSpecification planeSpec) {
		this.planeSpec = planeSpec;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((planeSpec == null) ? 0 : planeSpec.hashCode());
		result = prime * result + ((tailNumber == null) ? 0 : tailNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Plane other = (Plane) obj;
		if (planeSpec == null) {
			if (other.planeSpec != null)
				return false;
		} else if (!planeSpec.equals(other.planeSpec))
			return false;
		if (tailNumber == null) {
			if (other.tailNumber != null)
				return false;
		} else if (!tailNumber.equals(other.tailNumber))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Plane [tailNumber=" + tailNumber + ", planeSpec=" + planeSpec;
	}
}
