package by.dzmitry.belavia.entity;

import java.util.ArrayList;

import by.dzmitry.belavia.parser.action.CollectPlane;

public class PlaneCollection {

	private ArrayList<Plane> planes;

	public PlaneCollection(String xmlTag, String filename, String[] fieldsXML, PlaneSpecificatonCollection planeSpec) {

		this.planes = CollectPlane.collect(xmlTag, filename, fieldsXML, planeSpec);

	}

	public boolean isEmpty() {

		return this.planes.isEmpty();
	}

	public int size() {

		return this.planes.size();
	}

	public Plane get(int key) {
		return this.planes.get(key);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((planes == null) ? 0 : planes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlaneCollection other = (PlaneCollection) obj;
		if (planes == null) {
			if (other.planes != null)
				return false;
		} else if (!planes.equals(other.planes))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PlaneList [planes=" + planes + "]";
	}
}