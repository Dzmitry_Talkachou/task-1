package by.dzmitry.belavia.entity;

public class PassengerPlane extends Plane {

	private int passengers;

	public int getPassengers() {
		return passengers;
	}

	public void setPassengers(int passengers) {
		this.passengers = passengers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + passengers;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PassengerPlane other = (PassengerPlane) obj;
		if (passengers != other.passengers)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", passengers=" + passengers + "]\n";
	}
}
