package by.dzmitry.belavia.entity;

public class CargoPlaneSpecificaton extends PlaneSpecification {

	private int volume;
	private int cargo;

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public int getCargo() {
		return cargo;
	}

	public void setCargo(int cargo) {
		this.cargo = cargo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + cargo;
		result = prime * result + volume;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CargoPlaneSpecificaton other = (CargoPlaneSpecificaton) obj;
		if (cargo != other.cargo)
			return false;
		if (volume != other.volume)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", cargo=" + cargo + ", volume=" + volume + "]\n";
	}
}
