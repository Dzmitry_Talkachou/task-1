package by.dzmitry.belavia.entity;

public class CargoPlane extends Plane {

	private int loadedCargo;
	private int loadedVolume;

	public int getLoadedCargo() {
		return loadedCargo;
	}

	public void setLoadedCargo(int loadedCargo) {
		this.loadedCargo = loadedCargo;
	}

	public int getLoadedVolume() {
		return loadedVolume;
	}

	public void setLoadedVolume(int loadedVolume) {
		this.loadedVolume = loadedVolume;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + loadedCargo;
		result = prime * result + loadedVolume;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CargoPlane other = (CargoPlane) obj;
		if (loadedCargo != other.loadedCargo)
			return false;
		if (loadedVolume != other.loadedVolume)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", loadedCargo=" + loadedCargo + ", loadedVolume=" + loadedVolume + "]\n";
	}

}
