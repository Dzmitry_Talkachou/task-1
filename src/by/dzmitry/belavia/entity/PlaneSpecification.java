package by.dzmitry.belavia.entity;

public abstract class PlaneSpecification {

	private String name;
	private int fuel;
	private int fuelConsumption;
	private int speed;
	private int specificationId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

	public int getFuelConsumption() {
		return fuelConsumption;
	}

	public void setFuelConsumption(int fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getSpecificationId() {
		return specificationId;
	}

	public void setSpecificationId(int specId) {
		this.specificationId = specId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + fuel;
		result = prime * result + fuelConsumption;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + specificationId;
		result = prime * result + speed;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlaneSpecification other = (PlaneSpecification) obj;
		if (fuel != other.fuel)
			return false;
		if (fuelConsumption != other.fuelConsumption)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (specificationId != other.specificationId)
			return false;
		if (speed != other.speed)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PlaneSpec [name=" + name + ", fuel=" + fuel + ", fuelConsumption=" + fuelConsumption + ", speed="
				+ speed + ", specId=" + specificationId;
	}

}
