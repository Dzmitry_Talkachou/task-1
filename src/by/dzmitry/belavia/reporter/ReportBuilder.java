package by.dzmitry.belavia.reporter;

import static by.dzmitry.belavia.parser.constant.ParserConstant.*;

import by.dzmitry.belavia.entity.Airline;
import by.dzmitry.belavia.service.PlaneService;

public class ReportBuilder {

	private Airline airline;
	private PlaneService ps = new PlaneService();
	private final static String BR = "\n";
	private String report = BR;

	public ReportBuilder(Airline airline) {
		this.airline = airline;
		report += ("Airline name:" + this.airline.getName()) + BR;
	}

	public ReportBuilder buildMaxCargoAllCargoPlanes() {
		report += ("Maximum capacity of all cargo planes: " + ps.allMaxCargo(this.airline.getCargoPlanes()) + " kg") + BR;

		return this;
	}

	public ReportBuilder buildLoadedCargoAllCargoPlanes() {
		report += ("Loaded capacity of  cargo planes: " + ps.allCagroLoad(this.airline.getCargoPlanes()) + " kg") + BR;
		return this;
	}

	public ReportBuilder buildVolumeAllCargoPlanes() {
		report += ("Volume of all cargo planes: " + ps.allMaxVolume(this.airline.getCargoPlanes()) + " L") + BR;
		return this;
	}

	public ReportBuilder buildLoadedVolumeAllCargoPlanes() {
		report += ("Loaded volume of �apacity in all cargo planes: " + ps.allVolumeLoad(this.airline.getCargoPlanes())
				+ " kg") + BR;
		return this;
	}

	public ReportBuilder buildNumberPassengerSeats() {
		report += ("The number of passengers who have bought a ticket in all passenger planes: "
				+ ps.allPassengers(this.airline.getPassengerPlanes()) + " peoples") + BR;
		return this;
	}

	public ReportBuilder buildNumberPassengers() {
		report += ("The number of passenger seats in all planes: "
				+ ps.allPassengerSeats(this.airline.getPassengerPlanes()) + " pcs") + BR;
		return this;
	}

	public ReportBuilder buildFuelConsumptonCargoPlanesCheckedRange(int min, int max) {
		report += ("List of checked range (fuel consumption " + min + " ... " + max + ") for cargo planes:"
				+ ps.sortRange(min, max, FUEL_CONSUMPTION, CARGO_PLANE_SPEC, this.airline.getCargoPlanes())) + BR;
		return this;
	}

	public ReportBuilder buildFuelConsumptonPassengerPlanesCheckedRange(int min, int max) {
		report += ("List of checked range (fuel consumption " + min + " ... " + max + ") for passenger planes:"
				+ ps.sortRange(min, max, FUEL_CONSUMPTION, PASSENGER_PLANE_SPEC, this.airline.getPassengerPlanes()))
				+ BR;
		return this;
	}

	public ReportBuilder buildSortPassengerPlanesRangeFlying() {
		report += ("Sorted list of Passenger planes by range flying: "
				+ ps.sortMaxRangeFlight(this.airline.getPassengerPlanes())) + BR;
		return this;

	}

	public ReportBuilder buildSortdCargoPlanesRangeFlying() {
		report += ("Sorted list of Cargo planes by range flying: "
				+ ps.sortMaxRangeFlight(this.airline.getPassengerPlanes())) + BR;
		return this;

	}

	public String done() {
		return report;

	}

}