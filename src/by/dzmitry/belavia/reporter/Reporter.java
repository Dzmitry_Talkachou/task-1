package by.dzmitry.belavia.reporter;

import org.apache.log4j.Logger;

public class Reporter {
	private static final Logger LOGGER = Logger.getLogger(Reporter.class);
	private String report;

	public Reporter(String report) {
		this.report = report;
	}

	public void console() {
		System.out.println(this.report);
	}

	public void logger() {
		LOGGER.info(this.report);
	}
}