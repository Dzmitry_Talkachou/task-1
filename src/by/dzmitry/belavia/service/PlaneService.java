package by.dzmitry.belavia.service;

import static by.dzmitry.belavia.parser.constant.ParserConstant.*;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import by.dzmitry.belavia.comaparator.ComparatorMap;
import by.dzmitry.belavia.entity.CargoPlane;
import by.dzmitry.belavia.entity.CargoPlaneSpecificaton;
import by.dzmitry.belavia.entity.PassengerPlane;
import by.dzmitry.belavia.entity.PassengerPlaneSpecification;
import by.dzmitry.belavia.entity.Plane;
import by.dzmitry.belavia.entity.PlaneCollection;
import by.dzmitry.belavia.entity.PlaneSpecification;
import by.dzmitry.belavia.entity.PlaneSpecificatonCollection;
import by.dzmitry.belavia.validator.Validator;

public class PlaneService {
	private static final Logger LOGGER = Logger.getLogger(PlaneService.class);

	public int allCagroLoad(PlaneCollection planes) {

		int volumeLoad = 0;
		for (int i = 0; i < planes.size(); i++) {
			Plane plane = planes.get(i);
			if (plane instanceof CargoPlane) {
				volumeLoad += ((CargoPlane) plane).getLoadedVolume();
			} else {
				LOGGER.error("The method allCagroLoad() is undefined for this class.");
			}

		}
		return volumeLoad;
	}

	public int allMaxCargo(PlaneCollection planes) {
		int cargoMax = 0;
		for (int i = 0; i < planes.size(); i++) {
			Plane plane = planes.get(i);
			PlaneSpecification planeSpec = plane.getPlaneSpec();
			if (planeSpec instanceof CargoPlaneSpecificaton) {
				cargoMax += ((CargoPlaneSpecificaton) planeSpec).getCargo();
			} else {
				LOGGER.error("The method allMaxCargo() is undefined for this class.");

			}
		}
		return cargoMax;
	}

	public int allVolumeLoad(PlaneCollection planes) {
		int cargoLoad = 0;
		for (int i = 0; i < planes.size(); i++) {
			Plane plane = planes.get(i);
			if (plane instanceof CargoPlane) {
				cargoLoad += ((CargoPlane) plane).getLoadedCargo();
			} else {
				LOGGER.error("The method allVolumeLoad() is undefined for this class.");

			}
		}
		return cargoLoad;
	}

	public int allMaxVolume(PlaneCollection planes) {
		int maxVolume = 0;
		for (int i = 0; i < planes.size(); i++) {
			Plane plane = planes.get(i);
			PlaneSpecification planeSpec = plane.getPlaneSpec();
			if (planeSpec instanceof CargoPlaneSpecificaton) {
				maxVolume += ((CargoPlaneSpecificaton) plane.getPlaneSpec()).getVolume();
			} else {
				LOGGER.error("The method allMaxVolume() is undefined for this class.");
			}
		}
		return maxVolume;
	}

	public int allPassengerSeats(PlaneCollection planes) {
		int numberSeats = 0;
		for (int i = 0; i < planes.size(); i++) {
			Plane plane = planes.get(i);
			PlaneSpecification planeSpec = plane.getPlaneSpec();
			if (planeSpec instanceof PassengerPlaneSpecification) {
				numberSeats += ((PassengerPlaneSpecification) planes.get(i).getPlaneSpec()).getPassengerSeats();
			} else {
				LOGGER.error("The method allPassengerSeats() is undefined for this class.");
			}
		}
		return numberSeats;
	}

	public int allPassengers(PlaneCollection planes) {
		int passengers = 0;
		for (int i = 0; i < planes.size(); i++) {
			Plane plane = planes.get(i);
			if (plane instanceof PassengerPlane) {
				passengers += ((PassengerPlane) planes.get(i)).getPassengers();
			} else {
				LOGGER.error("The method allPassengerSeats() is undefined for this class.");
			}
		}
		return passengers;
	}

	public PlaneSpecification takePlaneSpecById(int searchSpecId, PlaneSpecificatonCollection collection) {
		boolean find = false;
		int i = 0, specId = 0;
		PlaneSpecification planeSpec = null;
		do {
			specId = collection.get(i).getSpecificationId();
			if (searchSpecId == specId) {
				planeSpec = collection.get(i);
				find = true;
			}
		} while (++i < collection.size() && !find);

		if (planeSpec != null) {

			LOGGER.error("Specification ID not found, return null!");
		}

		return planeSpec;
	}

	public Map<String, Integer> sortRange(int min, int max, String fieldName, String type, PlaneCollection planes) {

		Map<String, Integer> findPlanes = new HashMap<String, Integer>();

		for (int i = 0; i < planes.size(); i++) {
			int check = 0;

			Plane plane = planes.get(i);

			switch (type) {

			case CARGO_PLANE:
				check = Integer.parseInt(readPlane(plane, fieldName)); // Check is not necessary (int <-- String <-- int).
				break;

			case PASSENGER_PLANE:
				check = Integer.parseInt(readPlane(plane, fieldName));
				break;

			case CARGO_PLANE_SPEC:
				check = Integer.parseInt(readSpec(plane.getPlaneSpec(), fieldName));
				break;

			case PASSENGER_PLANE_SPEC:
				check = Integer.parseInt(readSpec(plane.getPlaneSpec(), fieldName));
				break;

			default:
				LOGGER.error("Not valid plane type, type= " + type + ", return 0");
			}
			if (Validator.checkRange(check, min, max)) {
				findPlanes.put(plane.getTailNumber(), check);
			}
		}
		ComparatorMap comaratorMap = new ComparatorMap();
		findPlanes = comaratorMap.sortMap(findPlanes);

		return findPlanes;
	}

	public Map<String, Integer> sortMaxRangeFlight(PlaneCollection planes) {

		Map<String, Integer> map = new HashMap<String, Integer>();

		for (int i = 0; i < planes.size(); i++) {

			map.put(planes.get(i).getTailNumber(), calcRangeFlight(planes.get(i).getPlaneSpec()));
		}
		ComparatorMap comaratorMap = new ComparatorMap();
		Map<String, Integer> report = comaratorMap.sortMap(map);
		return report;
	}

	public int calcRangeFlight(PlaneSpecification spec) {

		int rangeFlight = 0;
		if (spec.getFuelConsumption() != 0) {
			rangeFlight = spec.getFuel() / spec.getFuelConsumption() * spec.getSpeed();
		} else {
			LOGGER.error("FuelConsumption = 0, division by zero.");
		}
		return rangeFlight;
	}

	public String readPlane(Plane plane, String fieldName) {

		String value = null;
		switch (fieldName) {
		case TAIL_NUMBER:
			value = plane.getTailNumber();
			break;

		case PASSENGERS:
			value = Integer.toString(((PassengerPlane) plane).getPassengers());
			break;

		case LOADED_CARGO:
			value = Integer.toString(((CargoPlane) plane).getLoadedCargo());
			break;

		case LOADED_VOLUME:
			value = Integer.toString(((CargoPlane) plane).getLoadedVolume());
			break;

		default:
			LOGGER.info("Field does not exist, field = " + fieldName);

		}
		return value;
	}

	public String readSpec(PlaneSpecification planeSpec, String fieldName) {

		String value = null;

		switch (fieldName) {

		case NAME_MODEL_PLANE:
			value = planeSpec.getName();
			break;

		case FUEL:
			value = Integer.toString(planeSpec.getFuel());
			break;

		case FUEL_CONSUMPTION:
			value = Integer.toString(planeSpec.getFuelConsumption());
			break;

		case SPEED:
			value = Integer.toString(planeSpec.getSpeed());
			break;

		case SPECIFICATION_ID:
			value = Integer.toString(planeSpec.getSpecificationId());
			break;

		case PASSENGER_SEATS:
			value = Integer.toString(((PassengerPlaneSpecification) planeSpec).getPassengerSeats());
			break;

		case VOLUME:
			value = Integer.toString(((CargoPlaneSpecificaton) planeSpec).getVolume());
			break;

		case CARGO:
			value = Integer.toString(((CargoPlaneSpecificaton) planeSpec).getCargo());
			break;

		default:
			LOGGER.info("Field does not exist, fieldName = " + fieldName);
		}

		return value;

	}

}