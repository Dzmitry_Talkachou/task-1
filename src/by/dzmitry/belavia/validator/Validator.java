package by.dzmitry.belavia.validator;

import org.apache.log4j.Logger;

public class Validator {
	private static final Logger LOGGER = Logger.getLogger(Validator.class);

	public static boolean checkRange(int check, int min, int max) {

		boolean value;

		if (min - 1 < check && check < max + 1) {
			value = true;

		} else {
			LOGGER.info("Valdation "+ check +" for Range(" + min + "..." + max + ")failed, return FALSE");
			value = false;

		}
		return value;
	}

}
