package by.dzmitry.belavia.runner;

import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

import by.dzmitry.belavia.entity.Airline;
import by.dzmitry.belavia.reporter.ReportBuilder;
import by.dzmitry.belavia.reporter.Reporter;

public class Runner {
	static {
		new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
	}

	public static void main(String[] args) {

		Airline belavia = new Airline("BelAvia");
		ReportBuilder reportBuilderConsole = new ReportBuilder(belavia);
		
		ReportBuilder reportBuilderLogger = new ReportBuilder(belavia);

		reportBuilderConsole.buildMaxCargoAllCargoPlanes()
							.buildLoadedCargoAllCargoPlanes()
							.buildVolumeAllCargoPlanes()
							.buildLoadedVolumeAllCargoPlanes()
							.buildNumberPassengers()
							.buildNumberPassengerSeats()
							.buildFuelConsumptonCargoPlanesCheckedRange(7000, 13000)
							.buildFuelConsumptonPassengerPlanesCheckedRange(7000, 13000)
							.buildSortdCargoPlanesRangeFlying()
							.buildSortPassengerPlanesRangeFlying();
		
		reportBuilderLogger.buildSortdCargoPlanesRangeFlying()
						   .buildSortPassengerPlanesRangeFlying();
							
		Reporter reporterConsole = new Reporter(reportBuilderConsole.done());
		Reporter reporterLogger = new Reporter(reportBuilderLogger.done());

		reporterConsole.console();
		reporterLogger.logger();
	}
}
