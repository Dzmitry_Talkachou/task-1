package by.dzmitry.belavia.parser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import static javax.xml.stream.XMLStreamConstants.*;

public class ParserXml {

	private static final Logger LOGGER = Logger.getLogger(ParserXml.class);

	public static ArrayList<ParsedContainer> createContainers(String searchItem, String path, String[] parameters) {

		XMLInputFactory factory = XMLInputFactory.newFactory();
		ArrayList<ParsedContainer> list = new ArrayList<ParsedContainer>();
		ArrayList<String> tagsXML = new ArrayList<String>();

		for (int i = 0; i < parameters.length; i++) {
			tagsXML.add(null);
		}

		ParsedContainer objXML = null;
		try {
			XMLStreamReader reader = factory.createXMLStreamReader(new FileInputStream(path));

			while (reader.hasNext()) {
				int res = reader.next();

				if (res == START_ELEMENT) {
					if (reader.getLocalName().equals(searchItem)) {
						objXML = new ParsedContainer();
					}
					for (int i = 0; i < parameters.length; i++) {
						if (reader.getLocalName().equals(parameters[i])) {
							tagsXML.set(i, reader.getLocalName());
						}
					}
				}
				if (res == END_ELEMENT) {
					if (reader.getLocalName().equals(searchItem)) {
						list.add(objXML);
					}
				}
				if (res == CHARACTERS) {
					for (int i = 0; i < parameters.length; i++) {
						if (tagsXML.get(i) != null) {
							objXML.putElement(tagsXML.get(i), reader.getText());
							tagsXML.set(i, null);
						}
					}
				}
			}
			reader.close();

		} catch (FileNotFoundException | XMLStreamException e) {
			LOGGER.error("File '" + path + "' not open.");
		}
		return list;
	}
}
