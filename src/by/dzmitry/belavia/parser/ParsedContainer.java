package by.dzmitry.belavia.parser;

import java.util.HashMap;

public class ParsedContainer {

	private HashMap<String, String> conatainerMap = new HashMap<String, String>();

	public void putElement(String elementName, String element) {
		this.conatainerMap.put(elementName, element);
	}

	public String takeElementValue(String key) {
		return this.conatainerMap.get(key);

	}

	public HashMap<String, String> getConatainerMap() {
		return conatainerMap;
	}

	public void setConatainerMap(HashMap<String, String> conatainerMap) {
		this.conatainerMap = conatainerMap;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((conatainerMap == null) ? 0 : conatainerMap.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParsedContainer other = (ParsedContainer) obj;
		if (conatainerMap == null) {
			if (other.conatainerMap != null)
				return false;
		} else if (!conatainerMap.equals(other.conatainerMap))
			return false;
		return true;
	}

}
