package by.dzmitry.belavia.parser.constant;

public class ParserConstant {

	// Path for resource folder.
	public static final String RESOURCE_PATH = "resource/";

	// XML files, planes.
	public static final String CARGO_PLANE_FILE = RESOURCE_PATH + "cargo_plane.xml";
	public static final String PASSENGER_PLANE_FILE = RESOURCE_PATH + "passenger_plane.xml";

	// XML files, plane specifications.
	public static final String CARGO_PLANE_SPEC_FILE = RESOURCE_PATH + "cargo_plane_specification.xml";
	public static final String PASSENGER_PLANE_SPEC_FILE = RESOURCE_PATH + "passenger_plane_specification.xml";

	// tag for search in the XML file.
	public static final String CARGO_PLANE = "cargoPlane";
	public static final String PASSENGER_PLANE = "passengerPlane";
	public static final String CARGO_PLANE_SPEC = "cargoPlaneSpec";
	public static final String PASSENGER_PLANE_SPEC = "passengerPlaneSpec";

	// XML fields.
	public static final String PASSENGERS = "passengers";
	public static final String PASSENGER_SEATS = "passengerSeats";
	public static final String TAIL_NUMBER = "tailNumber";
	public static final String SPECIFICATION_ID = "specificationId";
	public static final String NAME_MODEL_PLANE = "name";
	public static final String FUEL = "fuel";
	public static final String FUEL_CONSUMPTION = "fuelConsumption";
	public static final String SPEED = "speed";
	public static final String VOLUME = "volume";
	public static final String LOADED_CARGO = "loaded�argo";
	public static final String LOADED_VOLUME = "loadedVolume";
	public static final String CARGO = "cargo";
	public static final String TYPE = "type";

	// Array of constants who describe objects in XML file.
	public static final String[] CARGO_PLANE_SPEC_OPTIONS = { SPECIFICATION_ID, NAME_MODEL_PLANE, FUEL,
			FUEL_CONSUMPTION, SPEED, CARGO, VOLUME };

	public static final String[] PASSENGER_PLANE_SPEC_OPTIONS = { SPECIFICATION_ID, NAME_MODEL_PLANE, FUEL,
			FUEL_CONSUMPTION, SPEED, PASSENGER_SEATS };

	public static final String[] PASSENGER_PLANE_OPTIONS = { TAIL_NUMBER, SPECIFICATION_ID, PASSENGERS };

	public static final String[] CARGO_PLANE_OPTIONS = { TAIL_NUMBER, SPECIFICATION_ID, LOADED_CARGO, LOADED_VOLUME };

	// String pointer for validate
	public static final int STRING_POINTER = -578126; // S5 T7 R8 I1 N2 G6

	// Array for validate CARGO_PLANE_SPEC_OPTIONS
	public static final int[][] CARGO_PLANE_SPEC_OPTIONS_CHECK = { { 1, 1000 }, { STRING_POINTER, 0 },
			{ 20000, 300000 }, { 100, 25000 }, { 700, 1000 }, { 20000, 150000 }, { 20000, 150000 } };

	// Array for validate PASSENGER_PLANE_SPEC_OPTIONS
	public static final int[][] PASSENGER_PLANE_SPEC_OPTIONS_CHECK = { { 1, 1000 }, { STRING_POINTER, 0 },
			{ 20000, 300000 }, { 1000, 25000 }, { 700, 1000 }, { 20, 1000 } };

	// Array for validate CARGO_PLANE_OPTIONS
	public static final int[][] CARGO_PLANE_OPTIONS_CHECK = { { STRING_POINTER, 0 }, { 1, 1000 }, { 0, 150000 },
			{ 0, 150000 } };

	// Array for validate PASSENGER_PLANE_OPTIONS
	public static final int[][] PASSENGER_PLANE_OPTIONS_CHECK = { { STRING_POINTER, 0 }, { 1, 1000 }, { 20, 1000 } };

}
