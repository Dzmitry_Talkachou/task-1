package by.dzmitry.belavia.parser.action;

import static by.dzmitry.belavia.parser.constant.ParserConstant.*;

import org.apache.log4j.Logger;

import by.dzmitry.belavia.entity.CargoPlane;
import by.dzmitry.belavia.entity.PassengerPlane;
import by.dzmitry.belavia.entity.Plane;

public class FillPlane {

	private static final Logger LOGGER = Logger.getLogger(FillPlane.class);

	public void fill(Plane plane, String options, String fieldValue) {

		boolean possibleError = false;
		int fieldValueIneger = Integer.parseInt(fieldValue.replaceAll("[^0-9]", ""));
		if (!Integer.toString(fieldValueIneger).equals(fieldValue)) {
			possibleError = true;
		}

		switch (options) {
		case TAIL_NUMBER:
			plane.setTailNumber(fieldValue);
			break;

		case PASSENGERS:
			if (!possibleError) {
				((PassengerPlane) plane).setPassengers(fieldValueIneger);
			} else {
				LOGGER.error("Field '" + TAIL_NUMBER + "' must be integer.");
			}

			break;

		case LOADED_CARGO:
			if (!possibleError) {
				((CargoPlane) plane).setLoadedCargo(fieldValueIneger);
			} else {
				LOGGER.error("Field '" + LOADED_CARGO + "' must be integer.");
			}

			break;

		case LOADED_VOLUME:
			if (!possibleError) {
				((CargoPlane) plane).setLoadedVolume(fieldValueIneger);
			} else {
				LOGGER.error("Field '" + LOADED_VOLUME + "' must be integer.");
			}
			break;

		default:
			LOGGER.error("Field does not exist, options = " + options + ", param = " + fieldValue);

		}

	}

}
