package by.dzmitry.belavia.parser.action;

import static by.dzmitry.belavia.parser.constant.ParserConstant.*;

import java.util.Map;
import org.apache.log4j.Logger;

import by.dzmitry.belavia.parser.ParsedContainer;
import by.dzmitry.belavia.validator.Validator;

public class ValidatorParsedObject {

	private static final Logger LOGGER = Logger.getLogger(ValidatorParsedObject.class);

	public static boolean validate(String type, ParsedContainer container) {

		boolean value = true;

		switch (type) {

		case CARGO_PLANE:
			value = validateElement(CARGO_PLANE_OPTIONS, CARGO_PLANE_OPTIONS_CHECK, container);
			break;

		case CARGO_PLANE_SPEC:
			value = validateElement(CARGO_PLANE_SPEC_OPTIONS, CARGO_PLANE_SPEC_OPTIONS_CHECK, container);
			break;

		case PASSENGER_PLANE:
			value = validateElement(PASSENGER_PLANE_OPTIONS, PASSENGER_PLANE_OPTIONS_CHECK, container);
			break;

		case PASSENGER_PLANE_SPEC:
			value = validateElement(PASSENGER_PLANE_SPEC_OPTIONS, PASSENGER_PLANE_SPEC_OPTIONS_CHECK, container);
			break;

		default:
			value = false;
			LOGGER.error("Wrong (" + type + ") type of planes.");
		}
		if (!value) {
			LOGGER.error("make object - " + container.getConatainerMap() + " - " + value);
		}
		return value;
	}

	private static boolean validateElement(String[] options, int[][] optionsCheck, ParsedContainer container) {

		boolean value = true;
		Map<String, String> map = container.getConatainerMap();

		for (int i = 0; i < optionsCheck.length; i++) {
			String element = map.get(options[i]);

			int min = optionsCheck[i][0];
			int max = optionsCheck[i][1];

			if (min != STRING_POINTER) {
				if (value) {
					value = Validator.checkRange(Integer.valueOf(element), min, max);
				}
			}
		}
		return value;
	}

}
