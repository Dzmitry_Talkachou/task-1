package by.dzmitry.belavia.parser.action;

import static by.dzmitry.belavia.parser.constant.ParserConstant.*;

import java.util.ArrayList;
import org.apache.log4j.Logger;

import by.dzmitry.belavia.entity.Plane;
import by.dzmitry.belavia.entity.PlaneSpecification;
import by.dzmitry.belavia.entity.PlaneSpecificatonCollection;
import by.dzmitry.belavia.factory.PlaneFactory;
import by.dzmitry.belavia.parser.ParsedContainer;
import by.dzmitry.belavia.parser.ParserXml;
import by.dzmitry.belavia.service.PlaneService;

public class CollectPlane {

	private static final Logger LOGGER = Logger.getLogger(CollectPlane.class);

	public static ArrayList<Plane> collect(String xmlTag, String filename, String[] fieldsXML,
			PlaneSpecificatonCollection planeSpecs) {

		ArrayList<ParsedContainer> objectsXML = ParserXml.createContainers(xmlTag, filename, fieldsXML);
		ArrayList<Plane> planeList = new ArrayList<Plane>();

		boolean flagCreate = false;
		for (ParsedContainer elem : objectsXML) {
			boolean checkParsedObject = ValidatorParsedObject.validate(xmlTag, elem);

			if (checkParsedObject) {

				PlaneFactory planeCreator = new PlaneFactory();
				Plane plane = planeCreator.create(xmlTag);

				flagCreate = true;

				for (int i = 0; i < fieldsXML.length; i++) {

					if (fieldsXML[i].equals(SPECIFICATION_ID)) {

						PlaneService planeService = new PlaneService();
						PlaneSpecification planeSpec = planeService
								.takePlaneSpecById(Integer.valueOf(elem.takeElementValue(fieldsXML[i])), planeSpecs);

						plane.setPlaneSpec(planeSpec);

					} else {

						FillPlane fillPlane = new FillPlane();

						fillPlane.fill(plane, fieldsXML[i], elem.takeElementValue(fieldsXML[i]));
					}
				}
				planeList.add(plane);
			} else {
				LOGGER.error("Cargo plane specification object don't created.");
			}
		}
		if (!flagCreate) {
			LOGGER.error("Collection 'planes' don't created.");

		}
		return planeList;
	}
}
