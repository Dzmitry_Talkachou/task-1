package by.dzmitry.belavia.parser.action;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import by.dzmitry.belavia.entity.PlaneSpecification;
import by.dzmitry.belavia.factory.PlaneSpecificationFactory;
import by.dzmitry.belavia.parser.ParsedContainer;
import by.dzmitry.belavia.parser.ParserXml;

public class CollectPlaneSpecification {

	private static final Logger LOGGER = Logger.getLogger(CollectPlaneSpecification.class);

	public static ArrayList<PlaneSpecification> collect(String xmlTag, String filename, String[] fieldsXML) {

		ArrayList<ParsedContainer> objectsXML = ParserXml.createContainers(xmlTag, filename, fieldsXML);
		ArrayList<PlaneSpecification> planesSpecsList = new ArrayList<PlaneSpecification>();

		boolean flagCreate = false;
		for (ParsedContainer elem : objectsXML) {

			boolean checkParsedObject = ValidatorParsedObject.validate(xmlTag, elem);

			if (checkParsedObject) {
				PlaneSpecificationFactory planeSpecCreator = new PlaneSpecificationFactory();
				PlaneSpecification planeSpec = planeSpecCreator.create(xmlTag);

				flagCreate = true;
				for (int i = 0; i < fieldsXML.length; i++) {
					FillPlaneSpecification fillPlane = new FillPlaneSpecification();
					fillPlane.fill(planeSpec, fieldsXML[i], elem.takeElementValue(fieldsXML[i]));
				}
				planesSpecsList.add(planeSpec);
			} else {
				LOGGER.error("Cargo plane specification object don't created.");
			}
		}
		if (!flagCreate) {
			LOGGER.error("Collection 'planesSpecs' don't created.");

		}
		return planesSpecsList;
	}
}
