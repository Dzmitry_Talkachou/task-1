package by.dzmitry.belavia.parser.action;

import static by.dzmitry.belavia.parser.constant.ParserConstant.*;

import org.apache.log4j.Logger;

import by.dzmitry.belavia.entity.CargoPlaneSpecificaton;
import by.dzmitry.belavia.entity.PassengerPlaneSpecification;
import by.dzmitry.belavia.entity.PlaneSpecification;

public class FillPlaneSpecification {

	private static final Logger LOGGER = Logger.getLogger(FillPlaneSpecification.class);

	public void fill(PlaneSpecification planeSpec, String fieldName, String fieldValue) {

		boolean possibleError = false;
		int fieldValueIneger = Integer.parseInt(fieldValue.replaceAll("[^0-9]", ""));
		if (!Integer.toString(fieldValueIneger).equals(fieldValue)) {
			possibleError = true;
		}

		switch (fieldName) {
		case NAME_MODEL_PLANE:
			planeSpec.setName(fieldValue);
			break;

		case FUEL:
			if (!possibleError) {
				planeSpec.setFuel(fieldValueIneger);
			} else {
				LOGGER.error("Field '" + FUEL + "' must be integer.");
			}
			break;

		case FUEL_CONSUMPTION:
			if (!possibleError) {
				planeSpec.setFuelConsumption(fieldValueIneger);
			} else {
				LOGGER.error("Field '" + FUEL_CONSUMPTION + "' must be integer.");
			}
			break;

		case SPEED:
			if (!possibleError) {
				planeSpec.setSpeed(fieldValueIneger);
			} else {
				LOGGER.error("Field '" + SPEED + "' must be integer.");
			}
			break;

		case SPECIFICATION_ID:
			if (!possibleError) {
				planeSpec.setSpecificationId(fieldValueIneger);
			} else {
				LOGGER.error("Field '" + SPECIFICATION_ID + "' must be integer.");
			}
			break;

		case PASSENGER_SEATS:
			if (!possibleError) {
				((PassengerPlaneSpecification) planeSpec).setPassengerSeats(fieldValueIneger);
			} else {
				LOGGER.error("Field '" + PASSENGER_SEATS + "' must be integer.");
			}
			break;

		case VOLUME:
			if (!possibleError) {
				((CargoPlaneSpecificaton) planeSpec).setVolume(fieldValueIneger);
			} else {
				LOGGER.error("Field '" + VOLUME + "' must be integer.");
			}
			break;

		case CARGO:
			if (!possibleError) {
				((CargoPlaneSpecificaton) planeSpec).setCargo(fieldValueIneger);
			} else {
				LOGGER.error("Field '" + CARGO + "' must be integer.");
			}
			break;

		default:
			LOGGER.error("Field does not exist, options = " + fieldName + ", param = " + fieldValue);
		}
	}
}